from settings import config
import numpy as np
from os import walk, makedirs
from os.path import join, exists, basename
import cv2
from tflite_runtime.interpreter import Interpreter
import sys, traceback


def set_input_tensor(interpreter, image):
  tensor_index = interpreter.get_input_details()[0]['index']
  input_tensor = interpreter.tensor(tensor_index)()[0]
  input_tensor[:, :] = image


def classify_image(interpreter, image, top_k=1):
  """Returns a sorted array of classification results."""
  set_input_tensor(interpreter, image)
  interpreter.invoke()
  output_details = interpreter.get_output_details()[0]
  output = np.squeeze(interpreter.get_tensor(output_details['index']))

  # If the model is quantized (uint8 data), then dequantize the results
  if output_details['dtype'] == np.uint8:
    scale, zero_point = output_details['quantization']
    output = scale * (output - zero_point)

  ordered = np.argpartition(-output, top_k)
  return [(i, output[i]) for i in ordered[:top_k]]


def preprocess_image(img):
    if (img.shape[0] != 224 or img.shape[1] != 224):
        img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_NEAREST)
    img = (img / 127.5)
    img = img - 1
    img = np.expand_dims(img, axis=0)
    return img


if __name__ == '__main__':

    # Make new folder LOG_ACCURACY
    folder_path = config.LOG_ACCURACY.split('/' + basename(config.LOG_ACCURACY))[0]
    if not exists(folder_path):
        print("Making folder: " + folder_path)
        makedirs(folder_path)

    # load the trained model from disk
    print("[INFO] loading model and label binarizer...")
    print(config.MODEL_PATH_TEST_tflite)

    interpreter = Interpreter(model_path=config.MODEL_PATH_TEST_tflite)
    interpreter.allocate_tensors()
    _, height, width, _ = interpreter.get_input_details()[0]['shape']

    # Proccessing images
    print("[INFO] processing images...")

    total_files = 0
    label_counter = 0
    label_found = False

    with open(config.LOG_ACCURACY, 'w') as accuracy_file:
        for path, dirs, files in walk(config.INPUT_PATH):
            if not basename(path) == basename(config.INPUT_PATH):
                for file in files:
                    img = cv2.imread(join(path, file))
                    output = img.copy()

                    # Predict
                    preds = classify_image(interpreter, preprocess_image(img))

                    # Getting label
                    label_id, prob = preds[0]
                    label = config.CLASSES[label_id]

                    # Label found
                    label_name = basename(basename(path))
                    if label_name == label:
                        label_counter += 1

                    # draw the activity on the output frame
                    text = "Env: {}".format(label)
                    text = text + " " + str(round(prob * 100)) + "%"
                    cv2.putText(output, text, (35, 50), cv2.FONT_HERSHEY_SIMPLEX,
                                1.25, (0, 255, 0), 5)

                    # Make directory
                    if not exists(join(config.OUTPUT_PATH, label_name)):
                        makedirs(join(config.OUTPUT_PATH, label_name))

                    # Save image
                    cv2.imwrite(join(config.OUTPUT_PATH, label_name, file), output)

                    total_files += 1

                # Accuracy
                accuracy = (label_counter / total_files) * 100
                accuracy_file.write(basename(basename(path)) + ': ' + str(accuracy) + '%\n\n')
                print(basename(basename(path)) + ': ' + str(accuracy) + '%')

                try:
                    if accuracy < 70:
                        print("Accuracy less than 70%")
                        sys.exit(1)

                    total_files = 0
                    label_counter = 0
                except:
                    traceback.print_exc(file=sys.stdout)
                    raise


