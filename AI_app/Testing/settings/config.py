import os

CLASSES = ["Accidents", "Flood", "Guns", "violence"]
INPUT_PATH = os.path.sep.join(["Testing", "test_images"])
OUTPUT_PATH = os.path.sep.join(["Testing", "test_images_out"])
LOG_ACCURACY = os.path.sep.join(["Testing", "model_accuracy", "MobileNet_tflite", "accuracy.txt"])

#MODEL_PATH_TEST = os.path.sep.join(["Model", "AI_camera_model", "Adam_VGG16", "camera.model"])
MODEL_PATH_TEST_tflite = os.path.sep.join(["Model", "AI_camera_model", "MobileNet_tflite", "camera.model", "model.tflite"])