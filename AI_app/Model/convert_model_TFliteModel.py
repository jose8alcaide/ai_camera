import tensorflow as tf
from tensorflow.keras.models import load_model
from config_VGG16 import config
from os.path import exists, basename
import os

# Make new folder MODEL PATH TFLITE
folder_path = config.MODEL_PATH_TFLITE.split('/' + basename(config.MODEL_PATH_TFLITE))[0]
if not exists(folder_path):
    print("Making folder: " + folder_path)
    os.makedirs(folder_path)

model = load_model(config.MODEL_PATH)
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()
open(config.MODEL_PATH_TFLITE, "wb").write(tflite_model)