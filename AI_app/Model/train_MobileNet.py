# set the matplotlib backend so figures can be saved in the background
import matplotlib
matplotlib.use("Agg")

import cv2
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense,GlobalAveragePooling2D,Dropout,SeparableConv2D,BatchNormalization, Activation, Dense
from tensorflow.keras.applications.mobilenet import MobileNet
from tensorflow.keras.optimizers import Adam
from imutils import paths
import os
from os.path import exists, basename, join
import tensorflow as tf
import tensorflow
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import TensorBoard
import matplotlib.pyplot as plt
import numpy as np

# ======== SETTINGS ========
DATABASE_PATH      = r"Model/db_images"
NUM_CLASSES        = 4
EPOCHS             = 10
LEARNING_RATE      = 0.0005
MODEL_PATH         = r"Model/AI_camera_model/MobileNet/model.hdf5"
MODEL_PATH_TFLITE  = r"Model/AI_camera_model/MobileNet_tflite/camera.model/model.tflite"
LOG                = r"Model/AI_camera_model/MobileNet/logs/mobilenet"
TRAINING_PLOT_PATH = r"Model/AI_camera_model/MobileNet/training_plot.png"
# ======== SETTINGS ========


def preparing_database():
    print("[INFO] preparing database ...")
    imagePaths = list(paths.list_images(DATABASE_PATH))

    # loop over the image paths
    for imagePath in imagePaths:
        img = cv2.imread(imagePath)
        if (img.shape[0] != 224 or img.shape[1] != 224):
            img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_NEAREST)

        imagePath = imagePath.replace(basename(DATABASE_PATH), basename(DATABASE_PATH) + "_copy")

        folder_path = imagePath.split('/' + basename(imagePath))[0]
        if not exists(folder_path):
            print("Making folder: " + folder_path)
            os.makedirs(folder_path)

        cv2.imwrite(imagePath, img)


def create_model():
    print("[INFO] creating model ...")
    # Base model without Fully connected Layers
    base_model = MobileNet(include_top=False, weights='imagenet', input_shape=(224,224,3))
    x = base_model.output
    # Add some new Fully connected layers to
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024,activation='relu')(x)
    x = Dropout(0.25)(x)
    x = Dense(512,activation='relu')(x)
    x = Dropout(0.25)(x)
    preds = Dense(NUM_CLASSES, activation='softmax')(x)

    model = Model(inputs=base_model.input, outputs=preds)

    for layer in model.layers[:87]:
        layer.trainable=False
    for layer in model.layers[87:]:
        layer.trainable=True

    return model


def preparing_dataset():
    print("[INFO] preparing dataset ...")
    train_datagen = ImageDataGenerator(preprocessing_function=tensorflow.keras.applications.mobilenet.preprocess_input,
                                     validation_split=0.25)

    train_generator = train_datagen.flow_from_directory(join(DATABASE_PATH + '_copy', 'train'),
                                                     target_size=(224,224),
                                                     batch_size=64,
                                                     class_mode='categorical',
                                                     subset='training')


    validation_generator = train_datagen.flow_from_directory(
                                                    join(DATABASE_PATH + '_copy', 'valid'),
                                                    target_size=(224,224),
                                                    batch_size=64,
                                                    class_mode='categorical',
                                                    subset='validation') # set as validation data

    return train_generator, validation_generator


def compile_model(model):
    print("[INFO] compile model ...")
    decay_rate = LEARNING_RATE / EPOCHS
    opt = Adam(lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=decay_rate, amsgrad=False)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

    return model


def train(model, train_generator, validation_generator):
    print("[INFO] trainning ...")
    checkpoint = ModelCheckpoint(MODEL_PATH, monitor='val_loss', verbose=1, save_weights_only=False, save_best_only=True, mode='min')
    tfboard = TensorBoard(log_dir=LOG)
    callbacks_list = [checkpoint, tfboard]

    step_size_train = train_generator.n/train_generator.batch_size
    step_size_val = validation_generator.samples // validation_generator.batch_size
    history = model.fit_generator(generator=train_generator,
                       steps_per_epoch=step_size_train,
                       validation_data = validation_generator,
                       validation_steps =step_size_val,
                       callbacks = callbacks_list,
                       epochs=EPOCHS)

    return history


def convert_model_to_tflite():
    print("[INFO] saving model to .tflite ...")
    new_model= tf.keras.models.load_model(filepath=MODEL_PATH)

    tflite_converter = tf.lite.TFLiteConverter.from_keras_model(new_model)
    tflite_model = tflite_converter.convert()
    open(MODEL_PATH_TFLITE, "wb").write(tflite_model)


def save_history(H):
    print("[INFO] saving history ...")
    # construct a plot that plots and saves the training history
    N = np.arange(0, EPOCHS)
    plt.style.use("ggplot")
    plt.figure()
    plt.plot(N, H.history["loss"], label="train_loss")
    plt.plot(N, H.history["val_loss"], label="val_loss")
    plt.plot(N, H.history["accuracy"], label="train_acc")
    plt.plot(N, H.history["val_accuracy"], label="val_acc")
    plt.title("Training Loss and Accuracy")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss/Accuracy")
    plt.legend(loc="lower left")
    plt.savefig(TRAINING_PLOT_PATH)


if __name__ == '__main__':
    preparing_database()
    model = create_model()
    train_generator, validation_generator = preparing_dataset()
    model = compile_model(model)
    history = train(model, train_generator, validation_generator)
    convert_model_to_tflite()
    save_history(history)


