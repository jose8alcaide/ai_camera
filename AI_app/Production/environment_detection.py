import numpy as np
import cv2
from tflite_runtime.interpreter import Interpreter
import os

CLASSES = ["Accidents", "Flood", "Guns", "violence"]
MODEL_PATH_tflite = os.path.sep.join(["Model", "AI_camera_model", "MobileNet_tflite", "camera.model", "model.tflite"])
THRESHOLD = 0.8

class EnvironmentDetection:

    def __init__(self):
        self.interpreter = Interpreter(model_path=MODEL_PATH_tflite)
        self.interpreter.allocate_tensors()
        #_, height, width, _ = self.interpreter.get_input_details()[0]['shape']


    def set_input_tensor(self, image):
      tensor_index = self.interpreter.get_input_details()[0]['index']
      input_tensor = self.interpreter.tensor(tensor_index)()[0]
      input_tensor[:, :] = image


    def classify_image(self, image, top_k=1):
      self.set_input_tensor(image)
      self.interpreter.invoke()
      output_details = self.interpreter.get_output_details()[0]
      output = np.squeeze(self.interpreter.get_tensor(output_details['index']))

      if output_details['dtype'] == np.uint8:
        scale, zero_point = output_details['quantization']
        output = scale * (output - zero_point)

      ordered = np.argpartition(-output, top_k)

      return [(i, output[i]) for i in ordered[:top_k]]


    def preprocess_image(self, img):
        if (img.shape[0] != 224 or img.shape[1] != 224):
            img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_NEAREST)
        img = (img / 127.5)
        img = img - 1
        img = np.expand_dims(img, axis=0)
        return img


    def predict_environment(self, frame):
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # Predict
        preds = self.classify_image(self.preprocess_image(img))

        # Getting label
        label_id, prob = preds[0]
        label = CLASSES[label_id]

        return label, prob

    def drawEnvironmentInFrame(self, frame, label, prob):

        if (prob > THRESHOLD):
            text = "Env: {}".format(label)
            text = text + " " + str(round(prob * 100)) + "%"

            cv2.putText(frame, text, (35, 50), cv2.FONT_HERSHEY_SIMPLEX,
                        1.25, (0, 255, 0), 5)

        return frame