from flask import Flask, render_template, Response
from camera import CameraStream
import cv2
from object_detection import ObjectDetection
from environment_detection import EnvironmentDetection


app = Flask(__name__)

cap = CameraStream().start()

obj = ObjectDetection()
env = EnvironmentDetection()

CLASSES = ["Accidents", "Flood", "Guns", "violence"]
THRESHOLD = 0.8

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')


def alert(frame, label_env, obj_labels):
    # Accidents
    if (label_env == CLASSES[0] and
        (any(label in 'person' for label in obj_labels) or
        any(label in 'car' for label in obj_labels) )):
        cv2.putText(frame, "ALERT", (150, 240), cv2.FONT_HERSHEY_SIMPLEX, 4, (0, 0, 255), 5)
    # Flood
    elif label_env == CLASSES[1] and any(label in 'person' for label in obj_labels):
        cv2.putText(frame, "ALERT", (150, 240), cv2.FONT_HERSHEY_SIMPLEX, 4, (0, 0, 255), 5)
    # Guns
    elif label_env == CLASSES[2] and any(label in 'person' for label in obj_labels):
        cv2.putText(frame, "ALERT", (150, 240), cv2.FONT_HERSHEY_SIMPLEX, 4, (0, 0, 255), 5)
    # violence
    elif label_env == CLASSES[3] and any(label in 'person' for label in obj_labels):
        cv2.putText(frame, "ALERT", (150, 240), cv2.FONT_HERSHEY_SIMPLEX, 4, (0, 0, 255), 5)

    return frame


def gen_frame():
    """Video streaming generator function."""
    while cap:
        frame = cap.read()

        # Predict
        boxes, classes, scores = obj.predictObjects(frame)
        label, prob = env.predict_environment(frame)

        # Draw environment and objects
        image, frame_labels = obj.drawObjectsInFrame(frame, boxes, classes, scores)
        image = env.drawEnvironmentInFrame(image, label, prob)

        # Alert
        if (prob > THRESHOLD):
            image = alert(image, label, frame_labels)

        convert = cv2.imencode('.jpg', image)[1].tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + convert + b'\r\n') # concate frame one by one and show result


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen_frame(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)