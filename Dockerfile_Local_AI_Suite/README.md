# Docker AI-Suite
## Summary:
In this repository you could find a docker with which you could run your own python code from wherever local path in your system.
This docker is available in two versions: gpu and cpu.
## Prerequisites:
- Docker 19.03.2 or upper.
- OS: Linux (only gpu tag),  all (cpu tag).
- GPU: Nvidia (only gpu tag)
- Nvidia drivers (only gpu tag)  
- nvidia-docker (only gpu tag)

## GPU:
### Content:
- Ubuntu 18.04
- Cuda 10.0
- cudnn 7 
- Python 3.7
- Tensorflow-gpu
- Keras
- matplotlib
- sklearn
- Pillow
- imutils
- OpenCV

#### Download:
~~~
docker pull jose3894/ai-suite:gpu
~~~
#### Run:
~~~
docker run --gpus all -v [local_path_application_py]:/app jose3894/ai-suite:gpu 'name_python_code.py'
~~~
If you do not specific any name then the default name will be 'AI-app.py'
#### Example: 
~~~
docker run --gpus all -v /home/user/Documents/application:/app jose3894/ai-suite:gpu 'myapp.py'
~~~

### Dockerfile
[Dockerfile GPU](https://gitlab.com/jose8alcaide/ai_camera/-/blob/master/Dockerfile_Local_AI_Suite/Dockerfile_gpu)

## CPU:
### Content:
- Ubuntu 18.04
- Python 3.7
- Tensorflow
- Keras
- matplotlib
- sklearn
- Pillow
- imutils
- OpenCV

#### Download:
~~~
docker pull jose3894/ai-suite:cpu
~~~
#### Run:
~~~
docker run -v [local_path_application_py]:/app jose3894/ai-suite:cpu 'name_python_code.py'
~~~
If you do not specific any name then the default name will be 'AI-app.py'
#### Example:
~~~
docker run -v /home/user/Documents/application:/app jose3894/ai-suite:cpu 'myapp.py'
~~~

### Dockerfile
[Dockerfile CPU](https://gitlab.com/jose8alcaide/ai_camera/-/blob/master/Dockerfile_Local_AI_Suite/Dockerfile_cpu)


## Application:
Also, you could make an application and run it easier:
~~~
* Root:
sudo su

* Change the location:
cd /usr/local/bin

* Create and edit new file, example: model
nano model

* Add this lines:
#!/bin/bash
echo Creating Artificial Inteligence model

* Depend if you are using CPU or GPU you should add and configure one of these lines too:
docker run -v [local_path_aplication_py]:/app jose3894/ai-suite:cpu 'name_python_code.py'
* or 
docker run --gpus all -v [local_path_aplication_py]:/app jose3894/ai-suite:gpu 'name_python_code.py'

* Save and exit:
- Ctrl + o 
- enter
- Ctrl + x

* Give it execution permisson:
chmod +x model
~~~
Now, you could run your program if you type model in a terminal.